using UnityEngine;
using UnityEditor;

public class PlayerSettingsVO {

	public string settingsName;
	public int accelerometerFrequency;
	public bool allowedAutorotateToLandscapeLeft;
	public bool allowedAutorotateToLandscapeRight;
	public bool allowedAutorotateToPortrait;
	public bool allowedAutorotateToPortraitUpsideDown;
	public bool allowFullscreenSwitch;
	public string aotOptions;
	public ApiCompatibilityLevel apiCompatibilityLevel;
	public bool bakeCollisionMeshes;
	public string bundleIdentifier;
	public string bundleVersion;
	public bool captureSingleScreen;
	public string cloudProjectId;
	public ColorSpace colorSpace;
	public string companyName;
	public D3D11FullscreenMode d3d11FullscreenMode;
	public D3D9FullscreenMode d3d9FullscreenMode;
	public UIOrientation defaultInterfaceOrientation;
	public bool defaultIsFullScreen;
	public int defaultScreenHeight;
	public int defaultScreenWidth;
	public int defaultWebScreenHeight;
	public int defaultWebScreenWidth;
	public ResolutionDialogSetting displayResolutionDialog;
	public bool enableInternalProfiler;
}