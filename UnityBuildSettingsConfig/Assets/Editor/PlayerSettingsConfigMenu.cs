using UnityEngine;
using UnityEditor;
 
public class PlayerSettingsConfigMenu
{

    // Add a new menu item with hotkey CTRL-SHIFT-A
    [MenuItem("Tools/Player Settings Config %#a")]
    private static void NewMenuOption()
    {
        PlayerSettingsConfigWindow.ShowWindow();
    }
}