using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public class SavePlayerSettingsWindow : EditorWindow
{
	private static string fileName = "NewSettings";
	private static EditorWindow window;
	private static PlayerSettingsVO settingsVO;

	public static void ShowWindow(PlayerSettingsVO vo)
	{	
		//Show existing window instance. If one doesn't exist, make one.
		settingsVO = vo;
		window = EditorWindow.GetWindowWithRect(typeof(SavePlayerSettingsWindow), new Rect(100,100,340,40), true, "Save Settings");		
	}
	
	void OnGUI()
	{	
		fileName = GUI.TextField(new Rect(10, 10, Screen.width - 100, 25), fileName, 25);

		if(GUI.Button(new Rect(Screen.width - 100, 10, 90, 25), "Save")){
			settingsVO.settingsName = fileName;
			PlayerSettingsConfigWindow.SaveFile(settingsVO);
			window.Close();
		}
	}
}