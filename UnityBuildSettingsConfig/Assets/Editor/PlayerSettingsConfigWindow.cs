using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public class PlayerSettingsConfigWindow : EditorWindow
{
	private static List<PlayerSettingsVO> settingsCollection = new List<PlayerSettingsVO>();
	private static List<string> settingsFileNames = new List<string>();
	private static string configLocation = "SettingsConfigs/Resources";
	private static Vector2 scrollPosition;
	private static EditorWindow window;
	private static float buttonSize = 20;
	private static int selectedSettings = 0;
	private static Color defaultColor = Color.grey;
	private static Color selectedColor = Color.yellow;

	[MenuItem("Window/Player Settings Config Window")]
	public static void ShowWindow()
	{	
		//Show existing window instance. If one doesn't exist, make one.
		window = EditorWindow.GetWindow(typeof(PlayerSettingsConfigWindow), false, "Settings Config");

		if(settingsCollection.Count == 0){
			LoadSettings();
		}
	}

	public static void LoadSettings(){
		//load config file
		var configs = Resources.LoadAll<TextAsset>("");

		settingsFileNames.Clear();
		settingsCollection.Clear();
		foreach(TextAsset asset in configs){

			var vo = JsonUtility.FromJson<PlayerSettingsVO>(asset.text);
			settingsFileNames.Add(vo.settingsName);
			settingsCollection.Add(vo);
		}

		if(settingsCollection.Count > 0){
			ApplySettings(settingsCollection[0]);
		}
	}
	
	void OnGUI()
	{	
		//file list
		// Debug.Log(settingsFileNames.Count);
		// if(settingsFileNames.Count > 0){
		// 	EditorGUILayout.Popup("Label", 0, settingsFileNames.ToArray()); 
		// }
		if(window == null)
			return;

		scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, true, false, GUILayout.Width(Screen.width),
			GUILayout.Height(Screen.height - (buttonSize*4f)));

		GUI.color = defaultColor;

		int i = 0;
		foreach(string name in settingsFileNames){
			
			GUI.color = i == selectedSettings ? selectedColor : defaultColor;
			if(GUI.Button(new Rect(0,i * buttonSize,window.position.width,buttonSize), name)){
				selectedSettings = i;
				ApplySettings(settingsCollection[i]);
			}

			i++;
		}

		GUI.color = defaultColor;

		EditorGUILayout.EndScrollView();

		if(GUI.Button(new Rect(0,Screen.height - (buttonSize *4f),window.position.width,buttonSize), "Add")){
			Debug.Log("PlayerSettings: " + PlayerSettings.bundleIdentifier);

			var settingsVO = new PlayerSettingsVO();
			PopulateCurrentSettingsVO(settingsVO);
			SavePlayerSettingsWindow.ShowWindow(settingsVO);
		}

		if(GUI.Button(new Rect(0,Screen.height - (buttonSize*3f),window.position.width, buttonSize), "Update")){
			var settingsVO = settingsCollection[selectedSettings];
			PopulateCurrentSettingsVO(settingsVO);
			SaveFile(settingsVO);
		}

		if(GUI.Button(new Rect(0,Screen.height - (buttonSize*2f),window.position.width, buttonSize), "Delete")){
			var settingsVO = settingsCollection[selectedSettings];
			DeleteFile(settingsVO);
		}
	}

	private void PopulateCurrentSettingsVO(PlayerSettingsVO vo){

		vo.accelerometerFrequency = PlayerSettings.accelerometerFrequency;
		vo.allowedAutorotateToLandscapeLeft = PlayerSettings.allowedAutorotateToLandscapeLeft;
		vo.allowedAutorotateToLandscapeRight = PlayerSettings.allowedAutorotateToLandscapeRight;
		vo.allowedAutorotateToPortrait = PlayerSettings.allowedAutorotateToPortrait;
		vo.allowedAutorotateToPortraitUpsideDown = PlayerSettings.allowedAutorotateToPortraitUpsideDown;
		vo.allowFullscreenSwitch = PlayerSettings.allowFullscreenSwitch;
		vo.aotOptions = PlayerSettings.aotOptions;
		vo.apiCompatibilityLevel = PlayerSettings.apiCompatibilityLevel;
		vo.bundleIdentifier = PlayerSettings.bundleIdentifier;
		vo.bakeCollisionMeshes = PlayerSettings.bakeCollisionMeshes;
		vo.bundleIdentifier = PlayerSettings.bundleIdentifier;
		vo.bundleVersion = PlayerSettings.bundleVersion;
		vo.captureSingleScreen = PlayerSettings.captureSingleScreen;
		vo.companyName = PlayerSettings.companyName;
		vo.colorSpace = PlayerSettings.colorSpace;
		vo.d3d11FullscreenMode = PlayerSettings.d3d11FullscreenMode;
		vo.d3d9FullscreenMode = PlayerSettings.d3d9FullscreenMode;
		vo.defaultIsFullScreen = PlayerSettings.defaultIsFullScreen;
		vo.defaultScreenHeight = PlayerSettings.defaultScreenHeight;
		vo.defaultWebScreenHeight = PlayerSettings.defaultWebScreenHeight;
		vo.defaultWebScreenWidth = PlayerSettings.defaultWebScreenWidth;
		vo.displayResolutionDialog = PlayerSettings.displayResolutionDialog;
		vo.enableInternalProfiler = PlayerSettings.enableInternalProfiler;
		// vo.forceSingleInstance = PlayerSettings.forceSingleInstance;
	}

	private static void ApplySettings(PlayerSettingsVO vo){

		PlayerSettings.accelerometerFrequency = vo.accelerometerFrequency;
		PlayerSettings.allowedAutorotateToLandscapeLeft = vo.allowedAutorotateToLandscapeLeft;
		PlayerSettings.allowedAutorotateToLandscapeRight = vo.allowedAutorotateToLandscapeRight;
		PlayerSettings.allowedAutorotateToPortrait = vo.allowedAutorotateToPortrait;
		PlayerSettings.allowedAutorotateToPortraitUpsideDown = vo.allowedAutorotateToPortraitUpsideDown;
		PlayerSettings.allowFullscreenSwitch = vo.allowFullscreenSwitch;
		PlayerSettings.aotOptions = vo.aotOptions;
		PlayerSettings.apiCompatibilityLevel = vo.apiCompatibilityLevel;
		PlayerSettings.bakeCollisionMeshes = PlayerSettings.bakeCollisionMeshes;
		PlayerSettings.bundleIdentifier = vo.bundleIdentifier;
		PlayerSettings.bundleVersion = vo.bundleVersion;
		PlayerSettings.captureSingleScreen = vo.captureSingleScreen;
		PlayerSettings.companyName = vo.companyName;
		PlayerSettings.colorSpace = vo.colorSpace;
		PlayerSettings.d3d11FullscreenMode  = vo.d3d11FullscreenMode;
		PlayerSettings.d3d9FullscreenMode  = vo.d3d9FullscreenMode;
		PlayerSettings.defaultInterfaceOrientation = vo.defaultInterfaceOrientation;
		PlayerSettings.defaultScreenHeight = vo.defaultScreenHeight;
		PlayerSettings.defaultWebScreenHeight = vo.defaultWebScreenHeight;
		PlayerSettings.defaultWebScreenWidth = vo.defaultWebScreenWidth;
		PlayerSettings.displayResolutionDialog = vo.displayResolutionDialog;
		PlayerSettings.enableInternalProfiler = vo.enableInternalProfiler;
		
	}

	private void SaveCollection(){

		//Saving JSON locally
		foreach(PlayerSettingsVO vo in settingsCollection){
			SaveFile(vo);
		}
	}

	public static void SaveFile(PlayerSettingsVO vo){
		
		var json = JsonUtility.ToJson(vo, true);
		System.IO.File.WriteAllText(Application.dataPath + "/" + configLocation + "/" + vo.settingsName + ".txt", json);
		AssetDatabase.Refresh();
		LoadSettings();
	}

	public static void DeleteFile(PlayerSettingsVO vo){
		System.IO.File.Delete(Application.dataPath + "/" + configLocation + "/" + vo.settingsName + ".txt");
		AssetDatabase.Refresh();
		LoadSettings();
	}
}